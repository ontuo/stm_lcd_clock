/*
 * lcd.c
 *
 *  Created on: Mar 14, 2020
 *      Author: tuo
 */
#include "lcd.h"

struct LCD_character_part  part_1 = {.byte_0 = 0b00011111,
									 .byte_1 = 0b00011111,
									 .byte_2 = 0b00000011,
									 .byte_3 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00000011,
									 .byte_5 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00000011,
									 .byte_7 = 0b00000011};

struct LCD_character_part  part_2 = {.byte_0 = 0b00000011,
									 .byte_1 = 0b00000011,
									 .byte_2 = 0b00000011,
									 .byte_3 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00000011,
									 .byte_5 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011111,
									 .byte_7 = 0b00011111};

struct LCD_character_part  part_3 = {.byte_0 = 0b00000000,
									 .byte_1 = 0b00000000,
									 .byte_2 = 0b00000000,
									 .byte_3 = 0b00000000,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00000000,
									 .byte_5 = 0b00000000,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011111,
									 .byte_7 = 0b00011111};

struct LCD_character_part  part_4 = {.byte_0 = 0b00011111,
									 .byte_1 = 0b00011111,
									 .byte_2 = 0b00000000,
									 .byte_3 = 0b00000000,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00000000,
									 .byte_5 = 0b00000000,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00000000,
									 .byte_7 = 0b00000000};

struct LCD_character_part  part_5 = {.byte_0 = 0b00011111,
									 .byte_1 = 0b00011111,
									 .byte_2 = 0b00011000,
									 .byte_3 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00011000,
									 .byte_5 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011111,
									 .byte_7 = 0b00011111};

struct LCD_character_part  part_6 = {.byte_0 = 0b00011111,
									 .byte_1 = 0b00011111,
									 .byte_2 = 0b00000011,
									 .byte_3 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00000011,
									 .byte_5 = 0b00000011,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011111,
									 .byte_7 = 0b00011111};

struct LCD_character_part  part_7 = {.byte_0 = 0b00011111,
									 .byte_1 = 0b00011111,
									 .byte_2 = 0b00011000,
									 .byte_3 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00011000,
									 .byte_5 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011000,
									 .byte_7 = 0b00011000};

struct LCD_character_part  part_8 = {.byte_0 = 0b00011000,
									 .byte_1 = 0b00011000,
									 .byte_2 = 0b00011000,
									 .byte_3 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_4 = 0b00011000,
									 .byte_5 = 0b00011000,
 	 	 	 	 	 	 	 	 	 .byte_6 = 0b00011111,
									 .byte_7 = 0b00011111};

void LCD_write_pins(char data){
	set_enable_on();
	HAL_Delay(1);
	if (((data >> 3)&1)==1) {d7_set();} else {d7_reset();}
	if (((data >> 2)&1)==1) {d6_set();} else {d6_reset();}
	if (((data >> 1)&1)==1) {d5_set();} else {d5_reset();}
	if ((data&1)==1) {d4_set();} else {d4_reset();}
	set_enable_off();
	HAL_Delay(1);
}

void LCD_init(){
	HAL_Delay(15);

	LCD_write_pins(0b11);
	HAL_Delay(4);
	LCD_write_pins(0b11);
	HAL_Delay(1);
	LCD_write_pins(0b11);
	HAL_Delay(1);

	LCD_write_pins(0b10);
	HAL_Delay(1);

	LCD_send_char(0b00101000, 0);
	HAL_Delay(1);

	LCD_send_char(0b00001100, 0);
	HAL_Delay(1);

	LCD_send_char(0b00000110, 0);
	HAL_Delay(1);

	LCD.is_cursor_blinking = 0;
	LCD.is_cursor_on = 0;
	LCD.is_display_on = 1;

	LCD_set_custom_char_part(part_1, 0x40);
	LCD_set_custom_char_part(part_2, 0x48);
	LCD_set_custom_char_part(part_3, 0x50);
	LCD_set_custom_char_part(part_4, 0x58);
	LCD_set_custom_char_part(part_5, 0x60);
	LCD_set_custom_char_part(part_6, 0x68);
	LCD_set_custom_char_part(part_7, 0x70);
	LCD_set_custom_char_part(part_8, 0x78);

}

void LCD_send_char(char data, char mode){
	mode ? set_to_data() : set_to_command();
	char high_bits = data >> 4;
	LCD_write_pins(high_bits);
	LCD_write_pins(data);
}

void LCD_send_word(char* data){
	int len = strlen(data);
	for (int i = 0; i < len; i++){
		LCD_send_char(data[i], 1);
	}
}

void LCD_clear(){
	LCD_send_char(0b1, 0);
	HAL_Delay(2);
}

void LCD_cursor_to_home(){
	LCD_send_char(0b10, 0);
	HAL_Delay(2);
}

void LCD_set_cursor_direction(int direction){
	// direction = 1 forward, direction = 2 back
	direction ? LCD_send_char(0b110, 0) : LCD_send_char(0b100, 0);
}

void LCD_set_display(int state){
	// state = 1 - on, state = 0 - off
	char LCD_state = LCD_check_states();
	if (state){
		LCD_send_char(LCD_state |= 0b1100, 0);
	}
	if (!state){
		LCD_send_char(LCD_state &= 0b1011, 0);
	}
}

void LCD_set_cursor(int state){
	// state = 1 - on, state = 0 - off
	char LCD_state = LCD_check_states();
	if (state){
		LCD_send_char(LCD_state |= 0b1010, 0);
	}
	if (!state){
		LCD_send_char(LCD_state &= 0b1101, 0);
	}
}

void LCD_set_cursor_blinking(int state){
	// state = 1 - on, state = 0 - off
	char LCD_state = LCD_check_states();
	if (state){
		LCD_send_char(LCD_state |= 0b1001, 0);
	}
	if (!state){
		LCD_send_char(LCD_state &= 0b1110, 0);
	}
}

char LCD_check_states(){
	char LCD_state = 0b1111;
	if( !LCD.is_cursor_blinking) { LCD_state^=1; }
	if( !LCD.is_cursor_on) { LCD_state^=2; }
	if( !LCD.is_display_on) { LCD_state^=4; }
	return LCD_state;
}

void LCD_set_cursor_shift(){
	LCD_send_char(0b11000, 0);
	HAL_Delay(1);
}

void LCD_set_display_shift(){
	LCD_send_char(0b10100, 0);
	HAL_Delay(1);
}

void LCD_set_position(char x, char y){
	switch(y){
	case 0:
		LCD_send_char(x|0x80, 0);
		HAL_Delay(1);
		break;
	case 1:
		LCD_send_char((0x40+x)|0x80, 0);
		HAL_Delay(1);
		break;
	}
}

void LCD_set_custom_char_part(struct LCD_character_part part, char address){
	LCD_send_char(address, 0);
	HAL_Delay(1);
	LCD_send_char(part.byte_0, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_1, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_2, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_3, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_4, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_5, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_6, 1);
	HAL_Delay(1);
	LCD_send_char(part.byte_7, 1);
	HAL_Delay(1);
}

void LCD_set_custom_1(char x){
	LCD_set_position(x, 0);
	LCD_send_char(0, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(1, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(2, 1);
	HAL_Delay(1);
}

void LCD_set_custom_2(char x){
	LCD_set_position(x, 0);
	LCD_send_char(3, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(5, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(2, 1);
}

void LCD_set_custom_3(char x){
	LCD_set_position(x, 0);
	LCD_send_char(3, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(5, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(2, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(5, 1);
}

void LCD_set_custom_4(char x){
	LCD_set_position(x, 0);
	LCD_send_char(7, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(1, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(0, 1);
	HAL_Delay(1);
}

void LCD_set_custom_5(char x){
	LCD_set_position(x, 0);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(3, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(2, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(5, 1);
}

void LCD_set_custom_6(char x){
	LCD_set_position(x, 0);
	LCD_send_char(6, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(3, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(5, 1);
	HAL_Delay(1);
}

void LCD_set_custom_7(char x){
	LCD_set_position(x, 0);
	LCD_send_char(3, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(0, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(0, 1);
	HAL_Delay(1);
}

void LCD_set_custom_8(char x){
	LCD_set_position(x, 0);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(5, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(5, 1);
	HAL_Delay(1);
}

void LCD_set_custom_9(char x){
	LCD_set_position(x, 0);
	LCD_send_char(4, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 0);
	LCD_send_char(5, 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char(2, 1);
	HAL_Delay(1);
	LCD_set_position(x+1, 1);
	LCD_send_char(1, 1);
	HAL_Delay(1);
}

void LCD_set_clock_dots(char x){
	LCD_set_position(x, 0);
	LCD_send_char('.', 1);
	HAL_Delay(1);
	LCD_set_position(x, 1);
	LCD_send_char('.', 1);
	HAL_Delay(1);
}
