#ifndef INC_LCD_H_
#define INC_LCD_H_

#include "main.h"
#include "string.h"

#define d4_set()				HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, 1);
#define d5_set()				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 1);
#define d6_set()				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1);
#define d7_set()				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);

#define d4_reset()				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
#define d5_reset()				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0);
#define d6_reset()				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 0);
#define d7_reset()				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);

#define set_enable_on()			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);
#define set_enable_off()		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);

#define set_to_data()			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1)
#define set_to_command()		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0)

#define screen_width			16
#define screen_lines			2

void LCD_init();
void LCD_send_char(char data, char mode);
void LCD_write_pins(char data);
void LCD_send_char(char data, char mode);
void LCD_send_word(char* data);
void LCD_clear();
char LCD_check_states();
void LCD_set_cursor_blinking(int state);
void LCD_set_cursor(int state);
void LCD_set_display(int state);
void LCD_set_cursor_direction(int direction);
void LCD_cursor_to_home();
void LCD_set_cursor_shift();
void LCD_set_display_shift();
void LCD_set_position(char x, char y);

void LCD_set_custom_1(char x);
void LCD_set_custom_2(char x);
void LCD_set_custom_3(char x);
void LCD_set_custom_4(char x);
void LCD_set_custom_5(char x);
void LCD_set_custom_6(char x);
void LCD_set_custom_7(char x);
void LCD_set_custom_8(char x);
void LCD_set_custom_9(char x);
void LCD_set_clock_dots(char x);

struct LCD_state{
	int is_display_on;
	int is_cursor_on;
	int is_cursor_blinking;
};

struct LCD_character_part{
	char byte_0;
	char byte_1;
	char byte_2;
	char byte_3;
	char byte_4;
	char byte_5;
	char byte_6;
	char byte_7;
};


struct LCD_state LCD;

struct LCD_character_part  part_1;
struct LCD_character_part  part_2;
struct LCD_character_part  part_3;
struct LCD_character_part  part_4;
struct LCD_character_part  part_5;
struct LCD_character_part  part_6;
struct LCD_character_part  part_7;
struct LCD_character_part  part_8;

void LCD_set_custom_char_part(struct LCD_character_part part, char address);

#endif /* INC_LCD_H_ */
